import './App.css';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import SignIn from './components/pages/SingIn';
import { Routes, Route, Navigate } from "react-router-dom";
import SignUp from './components/pages/SignUp';
import Todo from './components/pages/Todo';

function App() {

  return (
    <>
      <Routes>
        <Route path="/" element={<Navigate to="/signin" />} />
        <Route path="/signin" element={<SignIn />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/todo" element={<Todo />} />
      </Routes>      
    </>
  );
}

export default App;
