import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { AddTodo } from "../AddTodo";
import { Todos } from "../Todos";
import Box from "@mui/material/Box";

export default function Todo() {

    return (
        <>
            <Container maxWidth="sm">
                <Box
                    sx={{
                        marginTop: 10,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Typography
                        className='pt-100'
                        variant="h3"
                        gutterBottom>
                        Todos
                    </Typography>
                    <AddTodo />
                    <Todos />
                </Box>

            </Container>
        </>
    )
}

