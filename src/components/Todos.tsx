import { useSelector } from "react-redux/es/hooks/useSelector";
import { ITodoState } from "../models/todo/TodoState";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import { TextField } from "@mui/material";

export function Todos() {
    const todos = useSelector((state: ITodoState) => state.todos);
    console.log(todos);
    return (
        <>
            <List className="pt-100">
                {todos.map((todo, index) => {
                    return (
                        <>
                            <ListItem key={index}>
                                <TextField
                                    key={index}
                                    id="outlined-basic"
                                    variant="standard"
                                    style={{ width: 380 }}
                                    disabled
                                    value={todo.text} />
                            </ListItem>
                        </>
                    );
                })}
            </List>

        </>

    )
}