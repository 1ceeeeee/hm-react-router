import React, { useState } from "react"
import { useDispatch } from 'react-redux'
import { addTodo } from "../stateManagment/todoSlice"
import { nanoid } from "@reduxjs/toolkit"
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";

export function AddTodo() {
  const [input, setInput] = useState('');
  const dispatch = useDispatch();
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setInput(event.target.value);
  }
  const handleAddTodo = () => {
    dispatch(addTodo({
      id: nanoid(),
      text: input,
    }));
    setInput('');
  };

  return (
    <>    
      <Stack direction="row" spacing={2}>
        <TextField
          id="outlined-basic"
          label="Add Your todo"
          variant="outlined"
          style={{width: 300}}
          value={input}
          onChange={handleInputChange} />
        <Button
          type="button"
          variant="contained"
          onClick={handleAddTodo}>Add</Button>
      </Stack>
    </>
  )
}