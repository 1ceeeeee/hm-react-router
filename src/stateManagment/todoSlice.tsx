import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { ITodoState } from "../models/todo/TodoState"
import { ITodo } from "../models/todo/ITodo";

const initialState: ITodoState = {
    todos: [{id:"234wdg", text: 'Hi!'}],
}

export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        addTodo: (state, action: PayloadAction<ITodo>) => {
            state.todos.push(action.payload);
        },
        deleteTodo: (state, action: PayloadAction<ITodo>) => {
            state.todos = state.todos.filter((todo) =>
                todo.id !== action.payload.id);
        }
    }
});

export const { addTodo, deleteTodo } = todoSlice.actions;
export default todoSlice.reducer;