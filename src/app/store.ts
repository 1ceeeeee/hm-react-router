import {configureStore} from "@reduxjs/toolkit"
import todoReducer from "../stateManagment/todoSlice"

export const store = configureStore({
    reducer: todoReducer,
});
